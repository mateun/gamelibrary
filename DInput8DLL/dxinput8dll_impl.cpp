#define DXINPUT8DLL_EXPORTS
#include "dinput8dll.h"
#include <stdio.h>
#include "../GraphicsDLL/ggraphics.h"

#pragma comment(lib, "GraphicsDLL.lib")
#pragma comment(lib, "dxguid.lib")
#pragma comment(lib, "dinput8.lib")

DXInput8DLL::DXInput8DLL(GWindow* window) {
	HRESULT res = DirectInput8Create(GetModuleHandle(nullptr), DIRECTINPUT_VERSION, IID_IDirectInput8, (void**)&_dinput8, nullptr);
	if (FAILED(res)) {
		printf("failed to create directinput8\n");
		// TODO how to report to client	?		
	}

	HWND hwnd = window->getNativeWindow();

	// keyboard
	res = _dinput8->CreateDevice(GUID_SysKeyboard, &_diKeyboard, nullptr);
	if (FAILED(res)) { printf("failed to create keyboard device\n"); exit(1); }
	res = _diKeyboard->SetDataFormat(&c_dfDIKeyboard);
	if (FAILED(res)) { printf("failed to set default data format for keyboard\n"); exit(1); }
	res = _diKeyboard->SetCooperativeLevel(hwnd, DISCL_FOREGROUND | DISCL_NONEXCLUSIVE);
	if (FAILED(res)) { printf("failed to set cooperative level\n"); exit(1); }

	// mouse
	res = _dinput8->CreateDevice(GUID_SysMouse, &_diMouse, nullptr);
	if (FAILED(res)) { printf("failed to create mouse device\n"); exit(1); }
	res = _diMouse->SetDataFormat(&c_dfDIMouse);
	if (FAILED(res)) { printf("failed to set default data for mouse device\n"); exit(1); }
	res = _diMouse->SetCooperativeLevel(hwnd, DISCL_FOREGROUND | DISCL_NONEXCLUSIVE);
	if (FAILED(res)) { printf("failed to set cooperative level for mouse device\n"); exit(1); }

	ZeroMemory(&_diProps, sizeof(_diProps));
	_diProps.diph.dwSize = sizeof(DIPROPDWORD);
	_diProps.diph.dwHeaderSize = sizeof(DIPROPHEADER);
	_diProps.diph.dwObj = 0;
	_diProps.diph.dwHow = DIPH_DEVICE;
	// buffer size
	_diProps.dwData = 16;
	_diMouse->SetProperty(DIPROP_BUFFERSIZE, &_diProps.diph);
}

DXInput8DLL::~DXInput8DLL()
{
	// TODO: cleanup things
}

void DXInput8DLL::pollKeyboard() {
	HRESULT res = _diKeyboard->GetDeviceState(sizeof(_keyState), (void**)&_keyState);
	if (FAILED(res)) {
		_diKeyboard->Acquire();
	}

}

bool DXInput8DLL::keyPressed(KEY key)
{
	switch (key) {
	case KEY::W: return _keyState[DIK_W];
	case KEY::A: return _keyState[DIK_A];
	case KEY::S: return _keyState[DIK_S];
	case KEY::D: return _keyState[DIK_D];
	case KEY::R: return _keyState[DIK_R];
	case KEY::F: return _keyState[DIK_F];
	case KEY::E: return _keyState[DIK_E];
	case KEY::Q: return _keyState[DIK_Q];
	default: return false;
	}

}

DXINPUT8DLL_API GInput* instantiate(GWindow* win)
{
	return new DXInput8DLL(win);
}