#pragma once

#include "../InputDLL/InputDLL.h"
#ifdef DXINPUT8DLL_EXPORTS
#define DXINPUT8DLL_API __declspec(dllexport)
#else
#define DXINPUT8DLL_API __declspec(dllimport)
#endif

#include <dinput.h>
class GWindow;

class DXInput8DLL : public GInput {

public: 
	DXInput8DLL(GWindow* );
	virtual ~DXInput8DLL();
	void pollKeyboard() override;
	bool keyPressed(KEY key) override;

private:
	LPDIRECTINPUT8 _dinput8;
	LPDIRECTINPUTDEVICE8 _diKeyboard = nullptr;
	LPDIRECTINPUTDEVICE8 _diMouse = nullptr;
	DIPROPDWORD _diProps;
	bool _keyState[256];
};

extern "C" {
	DXINPUT8DLL_API GInput* instantiate(GWindow*);
}
