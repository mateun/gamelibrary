// The following ifdef block is the standard way of creating macros which make exporting
// from a DLL simpler. All files within this DLL are compiled with the DX11RENDERERDLL_EXPORTS
// symbol defined on the command line. This symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see
// DX11RENDERERDLL_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
#ifdef DX11RENDERERDLL_EXPORTS
#define DX11RENDERERDLL_API __declspec(dllexport)
#else
#define DX11RENDERERDLL_API __declspec(dllimport)
#endif

#include "../GraphicsDLL/ggraphics.h"
#include <d3d11.h>

// This class is exported from the dll
class DX11RENDERERDLL_API GDX11Renderer : public GRenderer {
public:
	virtual ~GDX11Renderer();
	void init(GWindow* window) override;
	void clearBlack() override;
	void present() override;
	void beginScene() override;
	void endScene() override;

private:
	
	ID3D11Device* _device;
	ID3D11DeviceContext* _ctx;
	IDXGISwapChain* _swapChain;
	ID3D11Debug* _debugger;
	ID3D11Texture2D* _backBuffer;
	ID3D11RenderTargetView* _rtv;
	ID3D11Texture2D* _depthStencilBuffer;
	ID3D11DepthStencilView* _depthStencilView;
};

extern "C" {
	DX11RENDERERDLL_API GRenderer* instantiate(GWindow*);
}
