// DX11RendererDLL.cpp : Defines the exported functions for the DLL.
//

#include "framework.h"
#include "DX11RendererDLL.h"
#include <stdio.h>
#include <d3d11.h>
#include <DirectXMath.h>
#include <DirectXPackedVector.h>
#include <DirectXColors.h>

#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "dxgi.lib")
#pragma comment(lib, "GraphicsDLL.lib")

DX11RENDERERDLL_API GRenderer* instantiate(GWindow* window)
{
    GRenderer* r = new GDX11Renderer();
    r->init(window);
    return r;
}

GDX11Renderer::~GDX11Renderer()
{
	printf("Before renderer cleanup\n");
	_debugger->ReportLiveDeviceObjects(D3D11_RLDO_DETAIL);
	_ctx->ClearState();
	_depthStencilView->Release();
	_depthStencilBuffer->Release();
	_rtv->Release();
	_backBuffer->Release();

	_swapChain->Release();

	_ctx->Release();
	_device->Release();
	printf("---------------------------------------------------------------------------------------------------------------------------------------\n");
	printf("After renderer cleanup\n");
	if (_debugger) _debugger->ReportLiveDeviceObjects(D3D11_RLDO_DETAIL);

	_debugger->Release();
}

void GDX11Renderer::init(GWindow* window)
{
    printf("in init of DX11 renderer.\n");

	IDXGIFactory* tmpFac = nullptr;
	HRESULT result = CreateDXGIFactory(__uuidof(IDXGIFactory), (void**)&tmpFac);
	if (FAILED(result)) {
		
		printf("IDXGI factory creation failed\n");
		exit(2);
	}
	IDXGIAdapter* adapter;
	result = tmpFac->EnumAdapters(0, &adapter);
	if (FAILED(result)) {
		printf("Adapter enum failed\n");
		exit(2);
	}

	IDXGIOutput* adapterOutput;
	result = adapter->EnumOutputs(0, &adapterOutput);
	if (FAILED(result)) {
		printf("Failed to create adapter output\n");
		exit(2);
	}

	UINT numModes;
	result = adapterOutput->GetDisplayModeList(DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_ENUM_MODES_INTERLACED, &numModes, nullptr);
	if (FAILED(result)) {
		printf("Failed to list modes\n");
		exit(2);
	}

	DXGI_MODE_DESC* modeList = new DXGI_MODE_DESC[numModes];
	result = adapterOutput->GetDisplayModeList(DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_ENUM_MODES_INTERLACED, &numModes, modeList);
	if (FAILED(result)) {
		printf("Failed to create mode list\n");
		exit(2);
	}

	UINT numerator = 0;
	UINT denumarator = 0;
	int w = window->width();
	int h = window->height();
	for (int i = 0; i < numModes; ++i) {
		if (modeList[i].Width == w) {
			if (modeList[i].Height == h) {
				numerator = modeList[i].RefreshRate.Numerator;
				denumarator = modeList[i].RefreshRate.Denominator;
				printf("Found valid refresh rate: %d %d\n", numerator, denumarator);
				break;
			}
		}
	}

	if (numerator == 0) {
		printf("Could not find valid refresh rate\n");
		exit(2);
	}

	delete[] modeList;
	modeList = nullptr;

	adapterOutput->Release();
	adapterOutput = nullptr;

	adapter->Release();
	adapter = nullptr;

	tmpFac->Release();
	tmpFac = nullptr;

	// end adapter querying

	printf("adpater querying finished.\n");

	D3D_FEATURE_LEVEL featureLevels = D3D_FEATURE_LEVEL_11_1;

	UINT cdfs = 0;

#ifdef _DEBUG
	cdfs |= D3D11_CREATE_DEVICE_DEBUG;
#endif

	result = D3D11CreateDevice(NULL, D3D_DRIVER_TYPE_HARDWARE, NULL, cdfs, &featureLevels, 1, D3D11_SDK_VERSION, &_device, NULL, &_ctx);
	if (FAILED(result)) {
		printf("create device failed.\n");
		exit(2);
	}

	UINT ql;
	_device->CheckMultisampleQualityLevels(DXGI_FORMAT_R8G8B8A8_UINT, 8, &ql);

	DXGI_SWAP_CHAIN_DESC scdesc;
	ZeroMemory(&scdesc, sizeof(scdesc));
	scdesc.BufferCount = 1;
	scdesc.BufferDesc.Height = h;
	scdesc.BufferDesc.Width = w;
	scdesc.Windowed = true;

	scdesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;

	scdesc.BufferDesc.RefreshRate.Numerator = numerator;
	scdesc.BufferDesc.RefreshRate.Denominator = denumarator;

	scdesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;

	scdesc.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
	scdesc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;

	scdesc.OutputWindow = window->getNativeWindow();
	scdesc.SampleDesc.Count = 4;	// samples per pixel
	scdesc.SampleDesc.Quality = 0;
	scdesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
	scdesc.Flags = 0;

	IDXGIDevice* pDXGIDevice = nullptr;
	result = _device->QueryInterface(__uuidof(IDXGIDevice), (void**)&pDXGIDevice);
	IDXGIAdapter* pDXGIAdapter = nullptr;
	result = pDXGIDevice->GetAdapter(&pDXGIAdapter);
	IDXGIFactory* pIDXGIFactory = nullptr;
	pDXGIAdapter->GetParent(__uuidof(IDXGIFactory), (void**)&pIDXGIFactory);

	result = pIDXGIFactory->CreateSwapChain(_device, &scdesc, &_swapChain);
	if (FAILED(result)) {
		printf("swapchain failed.\n");
		exit(1);
	}

	pIDXGIFactory->Release();
	pDXGIAdapter->Release();
	pDXGIDevice->Release();


	// Gather the debug interface
#ifdef _DEBUG
	_debugger = 0;
	result = _device->QueryInterface(__uuidof(ID3D11Debug), (void**)&_debugger);
	if (FAILED(result)) {
		printf("debugger creation failed\n");
		exit(1);
	}
#endif



	// Create a backbuffer
	result = _swapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (void**)&_backBuffer);
	if (FAILED(result)) {
		printf("backbuffer creation failed.\n");
		exit(1);
	}

	// Bind the backbuffer as our render target
	result = _device->CreateRenderTargetView(_backBuffer, NULL, &_rtv);
	if (FAILED(result)) {
		printf("rtv finished.\n");
		exit(1);
	}

	printf("swapchain created\n");

	// Create a depth/stencil buffer
	D3D11_TEXTURE2D_DESC td;
	td.Width = w;
	td.Height = h;
	td.MipLevels = 1;
	td.ArraySize = 1;
	td.Format = DXGI_FORMAT_D32_FLOAT;
	td.SampleDesc.Count = 4;
	td.SampleDesc.Quality = 0;
	td.Usage = D3D11_USAGE_DEFAULT;
	td.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	td.CPUAccessFlags = 0;
	td.MiscFlags = 0;

	result = _device->CreateTexture2D(&td, 0, &_depthStencilBuffer);
	if (FAILED(result)) {
		printf("D S failed.\n");
		exit(1);
	}

	D3D11_DEPTH_STENCIL_VIEW_DESC dpd;
	ZeroMemory(&dpd, sizeof(dpd));
	dpd.Flags = 0;
	dpd.Format = DXGI_FORMAT_D32_FLOAT;
	dpd.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DMS;

	result = _device->CreateDepthStencilView(_depthStencilBuffer, &dpd, &_depthStencilView);
	if (FAILED(result)) {
		printf("DS view creation failed.\n");
		exit(1);
	}

	printf("DS view created\n");

	D3D11_DEPTH_STENCIL_DESC depthStencilDesc;
	depthStencilDesc.DepthEnable = TRUE;
	depthStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	depthStencilDesc.DepthFunc = D3D11_COMPARISON_LESS;
	depthStencilDesc.StencilEnable = FALSE;
	depthStencilDesc.StencilReadMask = 0xFF;
	depthStencilDesc.StencilWriteMask = 0xFF;

	// Stencil operations if pixel is front-facing
	depthStencilDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
	depthStencilDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

	// Stencil operations if pixel is back-facing
	depthStencilDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
	depthStencilDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

	// Setting initial viewport
	D3D11_VIEWPORT vp;
	ZeroMemory(&vp, sizeof(vp));
	vp.TopLeftX = 0;
	vp.TopLeftY = 0;
	vp.Width = w;
	vp.Height = h;
	vp.MinDepth = 0;
	vp.MaxDepth = 1;
	_ctx->RSSetViewports(1, &vp);

	printf("DX11 init done.\n");
}

void GDX11Renderer::clearBlack()
{
	// TODO actually make this black... 
	float clearColors[] = { 0, 0.5, 1, 1 };
	ID3D11RenderTargetView* rtvs[1] = { _rtv };
	_ctx->OMSetRenderTargets(1, rtvs, _depthStencilView);
	_ctx->ClearRenderTargetView(_rtv, clearColors);

	// clear our depth target as well
	_ctx->ClearDepthStencilView(_depthStencilView, D3D11_CLEAR_DEPTH, 1, 0);
}

void GDX11Renderer::present()
{
	_swapChain->Present(0, 0);
}

void GDX11Renderer::beginScene()
{
}

void GDX11Renderer::endScene()
{
}