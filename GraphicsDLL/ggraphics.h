#pragma once

#ifdef GRAPHICSDLL_EXPORTS
#define GRAPHICSDLL_API __declspec(dllexport)
#else
#define GRAPHICSDLL_API __declspec(dllimport)
#endif

#include <Windows.h>
#include <stdio.h>
#include <stdlib.h>

#define NYI printf("not yet implemented!\n"); exit(1);

enum class RendererType {
	DX9,
	DX11,
	DX12,
	OpenGL4,
	SW,
	Vulkan,
};

struct GColor {
	float r, g, b, a;
};

class GRAPHICSDLL_API GWindow {
public:
	GWindow(int w, int h, bool fullscreen);
	HWND getNativeWindow();
	MSG pollMessages();
	int width();
	int height();
	bool isFullscreen();

private: 
	HWND _hwnd;
	int _w;
	int _h;
	bool _fullscreen;
};

class GRenderer {
public:
	virtual void init(GWindow* window) = 0;
	virtual void clearBlack() = 0;
	virtual void present() = 0;
	virtual void beginScene() { NYI };
	virtual void endScene() { NYI };
	virtual void setPixel(int x, int y, GColor color) { NYI };
	virtual void drawLine(int xStart, int yStart, int xEnd, int yEnd, GColor color) { NYI };


};


GRAPHICSDLL_API GRenderer* createRenderer(RendererType, GWindow*);
