#include "ggraphics.h"
#include <Windows.h>
#include <string>


GRenderer* createRenderer(GWindow* window, const std::wstring& rendererName) {
	const std::wstring dllName = rendererName + L"RendererDLL.dll";
	HMODULE dll = LoadLibrary(dllName.c_str());
	if (!dll) {
		return nullptr;
	}
	typedef GRenderer* (*CREATEFUNC)(GWindow*);
	CREATEFUNC fun;
	fun = (CREATEFUNC)GetProcAddress(dll, "instantiate");
	if (fun) {
		return fun(window);
	}
}

GRAPHICSDLL_API GRenderer* createRenderer(RendererType rendererType, GWindow* window) {
	switch (rendererType) {
	case RendererType::DX9: return createRenderer(window, L"DX9"); 
	case RendererType::DX11: return createRenderer(window, L"DX11");
	case RendererType::SW: return createRenderer(window, L"SW");

	default: return nullptr;
	}
}