#pragma once
#include "ggraphics.h"
#include <stdio.h>
#include <Windows.h>


LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
    case WM_PAINT:
    {
        PAINTSTRUCT ps;
        HDC hdc = BeginPaint(hWnd, &ps);
        // TODO: Add any drawing code that uses hdc here...
        EndPaint(hWnd, &ps);
    }
    break;
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

bool GWindow::isFullscreen() {
    return _fullscreen;
}

int GWindow::width() {
    return _w;
}

int GWindow::height() {
    return _h;
}

MSG GWindow::pollMessages() {
    MSG msg;
    if (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE))
    {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    return msg;
}

HWND GWindow::getNativeWindow() {
    return _hwnd;
}


GWindow::GWindow(int w, int h, bool fullscreen) : _w(w), _h(h), _fullscreen(fullscreen)
{
    printf("in window ctr.\n");
    HINSTANCE hInstance = GetModuleHandle(nullptr);

    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.lpszMenuName = nullptr;
    wcex.style = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc = WndProc;
    wcex.cbClsExtra = 0;
    wcex.cbWndExtra = 0;
    wcex.cbSize = sizeof(WNDCLASSEXW);
    wcex.hInstance = hInstance;
    wcex.hIcon = LoadIcon(NULL, IDI_APPLICATION);
    wcex.hIconSm = LoadIcon(nullptr, IDI_APPLICATION);
    wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
    wcex.lpszClassName = L"szWindowClass";

    if (!RegisterClassEx(&wcex)) {
        MessageBox(NULL, L"Window Registration Failed", L"Error", MB_ICONEXCLAMATION | MB_OK);
        return;
    }


    RECT crect;
    crect.left = 0;
    crect.bottom = h;
    crect.right = w;
    crect.top = 0;
    AdjustWindowRect(
        &crect,
        WS_CAPTION,
        false
    );

    HWND hWnd = CreateWindowW(L"szWindowClass", L"GameLibTest", WS_SYSMENU& ~WS_CAPTION,
        1000, 400, crect.right - crect.left, crect.bottom - crect.top, nullptr, nullptr, hInstance, nullptr);

    if (!hWnd)
    {
        // TODO what to do in the case window creation failed?
        // What to tell the caller?
    }
    else
    {
        _hwnd = hWnd;
        ShowWindow(hWnd, SW_SHOW);
        UpdateWindow(hWnd);
    }

   

    

}
