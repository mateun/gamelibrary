// The following ifdef block is the standard way of creating macros which make exporting
// from a DLL simpler. All files within this DLL are compiled with the SWRENDERERDLL_EXPORTS
// symbol defined on the command line. This symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see
// SWRENDERERDLL_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
#ifdef SWRENDERERDLL_EXPORTS
#define SWRENDERERDLL_API __declspec(dllexport)
#else
#define SWRENDERERDLL_API __declspec(dllimport)
#endif

#include "../GraphicsDLL/ggraphics.h"


// This class is exported from the dll
class SWRENDERERDLL_API SWRenderer : public GRenderer {
public:
	void init(GWindow* window) override;
	void clearBlack() override;
	void present() override;
	void setPixel(int x, int y, GColor col) override;
	void drawLine(int xStart, int yStart, int xEnd, int yEnd, GColor color) override;

private:
	void* _backBuffer = nullptr;
	BITMAPINFO _bitmapInfo;
	HDC _windowDC;
	GWindow* _window;
	
};

extern "C" {
	SWRENDERERDLL_API GRenderer* instantiate(GWindow*);
}
