// SWRendererDLL.cpp : Defines the exported functions for the DLL.
//

#include "framework.h"
#include "SWRendererDLL.h"
#include <stdio.h>

#pragma comment(lib, "GraphicsDLL.lib")

SWRENDERERDLL_API GRenderer* instantiate(GWindow* window)
{
    GRenderer* r = new SWRenderer();
    r->init(window);
    return r;
}

void SWRenderer::init(GWindow* window)
{
    _window = window;
    _windowDC = GetDC(_window->getNativeWindow());
    if (_backBuffer) {
        VirtualFree(nullptr, 0, MEM_RELEASE);
    }

    int w = _window->width();
    int h = _window->height();

    _bitmapInfo.bmiHeader.biSize = sizeof(_bitmapInfo.bmiHeader);
    _bitmapInfo.bmiHeader.biWidth = w;
    _bitmapInfo.bmiHeader.biHeight = h;
    _bitmapInfo.bmiHeader.biPlanes = 1;
    _bitmapInfo.bmiHeader.biBitCount = 32;
    _bitmapInfo.bmiHeader.biCompression = BI_RGB;

    int bytesPerPixel = 4;
    int bitMemorySize = (w * h) * bytesPerPixel;
    _backBuffer = VirtualAlloc(nullptr, bitMemorySize, MEM_COMMIT, PAGE_READWRITE);

}

void SWRenderer::clearBlack()
{
    //PatBlt(_windowDC, 0, 0, _window->width(), _window->height(), BLACKNESS);
    ZeroMemory(_backBuffer, _window->width() * _window->height() * 4);
}

void SWRenderer::present()
{

    

    // TODO maybe introduce some "Viewport" here
    // to allow for non fullscreen blitting
    // of the _backBuffer to the actual screen.
    StretchDIBits(_windowDC,
        0, 0, _window->width(), _window->height(),
        0, 0, _window->width(), _window->height(),
        _backBuffer,
        &_bitmapInfo,
        DIB_RGB_COLORS,
        SRCCOPY
    );

}

void SWRenderer::setPixel(int x, int y, GColor col)
{

    if (x < 0 || y < 0 || y >= _window->height() || x >= _window->width()) {
        return;
    }

    UINT32* bb = (UINT32*)_backBuffer;
    
    int offset = (y * _window->width()) + x;
    UINT32 pixel = 0;
    pixel = (int)(col.r * 255) << 16 | 
            (int)(col.g * 255) << 8  |
            (int)(col.b * 255) << 0;

    bb[offset] = pixel;
}

void SWRenderer::drawLine(int xStart, int yStart, int xEnd, int yEnd, GColor color)
{
    
    bool isXMajor = false;
    bool isYMajor = false;
    
    float dx = (float)xEnd - xStart;
    float dy = (float)yEnd - yStart;
    float gradient = (float)dy/dx;



    
    if (abs((int)dx) > abs((int)dy)) {
        isXMajor = true;
    }
    else {
        isYMajor = true;
        gradient = (float)dx/dy;
    }
    

    // Swap start and end coordinates
    // if we are negative. 
    if (dx < 0 && isXMajor) {
        int temp = xStart;
        xStart = xEnd;
        xEnd = temp;

        temp = yStart;
        yStart = yEnd;
        yEnd = temp;
    }

    if (dy < 0 && isYMajor) {
        int temp = yStart;
        yStart = yEnd;
        yEnd = temp;
    
        temp = xStart;
        xStart = xEnd;
        xEnd = temp;
    }
    

    if (isXMajor) 
    {
        float y = yStart;
        for (int x = xStart; x < xEnd; x++) {
            setPixel(x, y, color);
            y += gradient;
        }
    }
    // We are yMajor
    else 
    {
        float x = xStart;
        for (int y = yStart; y < yEnd; y++) {
            setPixel(x, y, color);
            x += gradient;
        }
    }

}

