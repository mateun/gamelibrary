// The following ifdef block is the standard way of creating macros which make exporting
// from a DLL simpler. All files within this DLL are compiled with the DX9RENDERERDLL_EXPORTS
// symbol defined on the command line. This symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see
// DX9RENDERERDLL_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
#ifdef DX9RENDERERDLL_EXPORTS
#define DX9RENDERERDLL_API __declspec(dllexport)
#else
#define DX9RENDERERDLL_API __declspec(dllimport)
#endif

#include "../GraphicsDLL/ggraphics.h"
#include <d3d9.h>

// This class is exported from the dll
class DX9RENDERERDLL_API GDX9Renderer : public GRenderer {
public:
	void init(GWindow* window) override;
	void clearBlack() override;
	void present() override;
	void beginScene() override;
	void endScene() override;

private:
	LPDIRECT3D9 _d3d9 = nullptr;
	IDirect3DDevice9* _device = nullptr;
	
};

extern "C" {
	DX9RENDERERDLL_API GRenderer* createDX9Renderer(GWindow*);
}
