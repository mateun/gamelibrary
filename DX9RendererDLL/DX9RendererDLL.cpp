// DX9RendererDLL.cpp : Defines the exported functions for the DLL.
//

#include "framework.h"
#include "DX9RendererDLL.h"
#include <stdio.h>
#include <d3d9.h>

#pragma comment(lib, "d3d9.lib")
#pragma comment(lib, "GraphicsDLL.lib")

DX9RENDERERDLL_API GRenderer* createDX9Renderer(GWindow* window)
{
    GRenderer* r = new GDX9Renderer();
    r->init(window);
    return r;
}



void GDX9Renderer::init(GWindow* window)
{
    _d3d9 = Direct3DCreate9(D3D_SDK_VERSION);
    if (!_d3d9) {
        printf("problem creating d3d9 object!\n");
        // TODO what?
    }

    D3DPRESENT_PARAMETERS pparams;
    ZeroMemory(&pparams, sizeof(pparams));
    pparams.SwapEffect = D3DSWAPEFFECT_DISCARD;
    pparams.hDeviceWindow = window->getNativeWindow();
    pparams.Windowed = !window->isFullscreen();
    pparams.BackBufferWidth = window->width();
    pparams.BackBufferHeight = window->height();
    pparams.BackBufferFormat = D3DFMT_A8R8G8B8;

    if (FAILED(_d3d9->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, window->getNativeWindow(), D3DCREATE_HARDWARE_VERTEXPROCESSING, &pparams, &_device)))
    {
        // TODO: exit?
        exit(1);
    }
}

void GDX9Renderer::clearBlack()
{
    D3DCOLOR clearColor = D3DCOLOR_XRGB(255, 0, 0xFF);
    _device->Clear(0, 0, D3DCLEAR_TARGET, clearColor, 1, 0);

    
}

void GDX9Renderer::present() {
    _device->Present(0, 0, 0, 0);
}

void GDX9Renderer::beginScene()
{
    _device->BeginScene();
}

void GDX9Renderer::endScene()
{
    _device->EndScene();
}

