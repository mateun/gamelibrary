// GameLibTestAppConsole.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include "../GraphicsDLL/ggraphics.h"
#include "../InputDLL/InputDLL.h"
#include "GameLibTestAppConsole.h"
#include <cassert>
#include <vector>


#pragma comment(lib, "GraphicsDLL.lib")
#pragma comment(lib, "InputDLL.lib")

namespace gmath {

    struct M4 {
        union {
            float elements[16];

            float e[16];
            struct {
                float m11;
                float m12;
                float m13;
                float m14;
                float m21;
                float m22;
                float m23;
                float m24;
                float m31;
                float m32;
                float m33;
                float m34;
                float m41;
                float m42;
                float m43;
                float m44;
            };
        };
        

        static M4 Zero() {
            M4 m;
            for (int i = 0; i < 16; i++) {
                m.elements[i] = 0;
            }

            return m;
        }

        static M4 id() {
            return One();
        }

        static M4 One() {
            M4 m = Zero();
            
            m.e[0] = 1;
            m.e[5] = 1;
            m.e[10] = 1;
            m.e[15] = 1;
            return m;
        }

        M4 operator* (const M4& other) 
        {
            M4 t = *this;
            M4 result = M4::Zero();
            const M4& o = other;

            result.m11 = m11 * o.m11 + m12 * o.m21 + m13 * o.m31 + m14 * o.m41;
            result.m12 = m11 * o.m12 + m12 * o.m22 + m13 * o.m32 + m14 * o.m42;
            result.m13 = m11 * o.m13 + m12 * o.m22 + m13 * o.m33 + m14 * o.m43;
            result.m14 = m11 * o.m14 + m12 * o.m24 + m13 * o.m34 + m14 * o.m44;


            result.m21 = m21 * o.m11 + m22 * o.m21 + m23 * o.m31 + m24 * o.m41;
            result.m22 = m21 * o.m12 + m22 * o.m22 + m23 * o.m32 + m24 * o.m42;
            result.m23 = m21 * o.m13 + m22 * o.m22 + m23 * o.m33 + m24 * o.m43;
            result.m24 = m21 * o.m14 + m22 * o.m24 + m23 * o.m34 + m24 * o.m44;

            result.m31 = m31 * o.m11 + m32 * o.m21 + m33 * o.m31 + m34 * o.m41;
            result.m32 = m31 * o.m12 + m32 * o.m22 + m33 * o.m32 + m34 * o.m42;
            result.m33 = m31 * o.m13 + m32 * o.m22 + m33 * o.m33 + m34 * o.m43;
            result.m34 = m31 * o.m14 + m32 * o.m24 + m33 * o.m34 + m34 * o.m44;

            result.m41 = m41 * o.m11 + m42 * o.m21 + m43 * o.m31 + m44 * o.m41;
            result.m42 = m41 * o.m12 + m42 * o.m22 + m43 * o.m32 + m44 * o.m42;
            result.m43 = m41 * o.m13 + m42 * o.m22 + m43 * o.m33 + m44 * o.m43;
            result.m44 = m41 * o.m14 + m42 * o.m24 + m43 * o.m34 + m44 * o.m44;
            
            return result;

        }
        
    };



    struct V3 {
        float x, y, z;
    };

    struct LINE {
        V3 start;
        V3 end;
    };

    struct TRI {
        V3 v1;
        V3 v2;
        V3 v3;
    };

    V3 add(V3 v1, V3 v2) {
        return { v1.x + v2.x, v1.y + v2.y, v1.z + v2.z };
    }

    V3 subtract(V3 v1, V3 v2) {
        return { v1.x - v2.x, v1.y - v2.y, v1.z - v2.z };
    }

    V3 multiplyScalar(V3 v, float scalar) {
        return { v.x * scalar, v.y * scalar, v.z * scalar };
    }

    V3 cross(V3 a, V3 b) {
        return { a.y * b.z - a.z * b.y, a.z * b.x - a.x * b.z, a.x * b.y - a.y * b.x };
    }

    float dot(V3 v1, V3 v2) {
        return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
    }

    V3 normalize(V3 input) 
    {
        float len = sqrt(input.x * input.x + input.y * input.y + input.z * input.z);
        return { input.x / len, input.y / len, input.z / len };
    }

    V3 viewportTransform(GWindow* window, V3 v) {
        V3 vvp = { 0, 0, 0 };
        vvp.x = window->width() / 2 * v.x + window->width() / 2;
        vvp.y = (window->height() / 2 * v.y + window->height() / 2);

        return vvp;
    }

    V3 perspective(V3 input)
    {
        float z = input.z;
        if (z == 0) {
            z = 0.0001;
        }
        return { input.x / (z * -1), input.y / (z *-1), input.z };
    }


    V3 translate(V3 input, V3 translation) 
    {
        return { input.x + translation.x, input.y + translation.y, input.z + translation.z };
    }

    V3 rotateAroundZ(V3 input, float angle) 
    {
        return { input.x * cos(angle) - input.y * sin(angle), input.x * sin(angle) + input.y * cos(angle), input.z};

    }

    V3 rotateAroundX(V3 input, float angle)
    {
        return { input.x, input.y * cos(angle) - input.z * sin(angle), input.y * sin(angle) + input.z * cos(angle), };
    }


    V3 rotateAroundY(V3 input, float angle)
    {
        return { input.x * cos(angle) + input.z * sin(angle), input.y, -input.x * sin(angle) + input.z * cos(angle) };
    }

    bool isTriangleTotallyInside(TRI in)
    {

        float f = -20;
        float n = -1;
        float w = -f * n;
        return 
            in.v1.x >= w && 
            in.v1.y >= w &&
            in.v1.z >= w && 
            in.v2.x >= w && 
            in.v2.y >= w &&
            in.v2.z >= w &&
            in.v3.x >= w && 
            in.v3.y >= w &&
            in.v3.z >= w;
    }

    bool isLineTotallyInside(LINE input)
    {
        float zFar = -30;
        return input.start.x > -1 && input.start.x < 1 &&
            input.start.y > -1 && input.start.y < 1 &&
            input.start.z < -1 && input.start.z > zFar &&
            input.end.x > -1 && input.end.x < 1 &&
            input.end.y > -1 && input.end.y < 1 &&
            input.end.z < -1 && input.end.z > zFar;
    }

    bool isLineTotallyOutside(LINE input) 
    {
        float zFar = -30;
        bool insideX = input.start.x > -1 && input.start.x < 1 && input.end.x > -1 && input.end.x < 1;
        bool insideY = input.start.y > -1 && input.start.y < 1 && input.end.y > -1 && input.end.y < 1;
        bool insideZ = input.start.z < -1 && input.start.z < zFar && input.end.z > -1 && input.end.z > zFar;

        return (!insideX && !insideY && insideZ);

    }

    struct Ray {
        V3 origin;
        V3 direction;
        float maxLength;

    };

    bool rayIntersectsPlane(V3 planeNormal, V3 planePoint, Ray ray, V3& intersectionPoint) {

        float rayPlaneOrientation = dot(ray.direction, planeNormal);
        if (abs(rayPlaneOrientation) <= 0.0001f) {
            // 0 would be a dot product of 0, which means they are normal to each other. 
            // If the ray is normal to the normal, it means effectively, ray and plane are parallel.
            // Due to rounding issues we declare them parallel if it is near �nough to zero.
            return false;
        }

        V3 rayToPlanePoint = subtract(planePoint, ray.origin);

        float t = dot(rayToPlanePoint, planeNormal) / rayPlaneOrientation;
        intersectionPoint = add(ray.origin, multiplyScalar(ray.direction, t));
        return t > 0;
    }


    // This takes a line as input
    // and clips it against all the 
    // 4 sides of the unit cube.
    LINE clipLineToUnitCube(LINE input) 
    {
        float zFar = -30;
        float zNear = -0.2;
        bool startInsideX = input.start.x > -1 && input.start.x < 1;
        bool startInsideY = input.start.y > -1 && input.start.y < 1;
        bool startInsideZ = input.start.z < zNear && input.start.z > zFar;

        bool endInsideX = input.end.x > -1 && input.end.x < 1;
        bool endInsideY = input.end.y > -1 && input.end.y < 1;
        bool endInsideZ = input.end.z < zNear && input.end.z > zFar;

        
        if (input.start.z > zNear) 
        {
            if (input.start.z < 0) {
                printf("startZ n error: %f (end.z): %f\n", input.start.z, input.end.z);
                Ray r = { input.start, subtract(input.end, input.start), 100 };
                V3 hp = { 0, 0, 0 };
                bool intersects = rayIntersectsPlane({ 0, 0, -zNear }, { 0, 0, zNear }, r, hp);
                if (intersects) {
                    printf("intersection point near plane: %f/%f/%f\n", hp.x, hp.y, hp.z);
                    return { hp, input.end };
                }
            }
            else {
                return { input.end, input.end };
            }
        }



        if (input.end.z > zNear)
        {
            printf("endZ n error: %f\n", input.end.z);
            Ray r = { input.end, subtract(input.start, input.end), 100 };
            V3 hp = { 0, 0, 0 };
            bool intersects = rayIntersectsPlane({ 0, 0, -zNear }, { 0, 0, zNear }, r, hp);
            if (intersects) {
                printf("intersection point near plane: %f/%f/%f\n", hp.x, hp.y, hp.z);
                return { input.start, hp };
            }
        }

       
        

        // TODO provide real result!!
        return input;


    }

    
    M4 matTrans(V3 t) {

        M4 result = M4::One();
        result.m14 = t.x;
        result.m24 = t.y;
        result.m34 = t.z;

        return result;
    }

    M4 matScale(V3 scale) {
        M4 result = M4::One();
        result.m11 = scale.x;
        result.m22 = scale.y;
        result.m33 = scale.z;
        
        return result;

    }

        
}

gmath::V3 translation = { 0, 0, -1.1 };
float rotZ = 0;
float rotY = 3.14589 / 2;
float rotX = 3.14589 / 4;


using namespace gmath;

// Pipeline

void rasterize(TRI tri, GWindow* window, GRenderer* renderer)
{
    // Do the perspective divide, 
    // and we are in NDC space.

    // Do the viewport transformation from there.
    // Draw the lines 
    // and later the pixels in between
    // to the backbuffer.

    printf("v2 before perspective divide: %f/%f/%f\n", tri.v2.x, tri.v2.y, tri.v2.z);

    V3 v1per = gmath::perspective(tri.v1);
    V3 v2per = gmath::perspective(tri.v2);
    V3 v3per = gmath::perspective(tri.v3);

    printf("v2 perspective coords: %f/%f/%f\n", v2per.x, v2per.y, v2per.z);
    
    
    gmath::V3 vvp1 = gmath::viewportTransform(window, v1per);
    gmath::V3 vvp2 = gmath::viewportTransform(window, v2per);
    gmath::V3 vvp3 = gmath::viewportTransform(window, v3per);

    renderer->drawLine(vvp1.x, vvp1.y, vvp2.x, vvp2.y, { 1, 0, 0, 1 });
    renderer->drawLine(vvp2.x, vvp2.y, vvp3.x, vvp3.y, { 0.8, 1, .8, 1 });
    renderer->drawLine(vvp3.x, vvp3.y, vvp1.x, vvp1.y, { 0.1, 0.1, 1, 1 });

}

void cullAndClipStage(TRI homogeneousTri, GWindow* win, GRenderer* renderer) 
{
    // cull
    if (!isTriangleTotallyInside(homogeneousTri)) {
        return;
    }

    float zNear = -1;

    TRI triClipped = homogeneousTri;

    if (homogeneousTri.v1.z > zNear ) {
        if (homogeneousTri.v2.z > zNear) {

            printf("needs clipping! v1.z v2.z %f %f\n", homogeneousTri.v1.z, homogeneousTri.v2.z);

            // Clip v1 and v2 to v3
            // Substitute v1 with new vertex: 
            V3 dirV1V3 = normalize(subtract(homogeneousTri.v3,homogeneousTri.v1));
            Ray r = { homogeneousTri.v1, dirV1V3, 100 };
            V3 hp = { 0, 0, 0 };
            bool intersects = rayIntersectsPlane({ 0, 0, -zNear }, { 0, 0, zNear }, r, hp);
            if (intersects) {
                triClipped.v1 = hp;
            }

            V3 dirV2V3 = normalize(subtract(homogeneousTri.v3, homogeneousTri.v2));
            r = { homogeneousTri.v2, dirV2V3, 100 };
            hp = { 0, 0, 0 };
            intersects = rayIntersectsPlane({ 0, 0, -zNear }, { 0, 0, zNear }, r, hp);
            if (intersects) {
                triClipped.v2 = hp;
            }

            triClipped.v3 = homogeneousTri.v3;

        }
        else if (homogeneousTri.v3.z > -1) {
            printf("only v3 needs to be clipped!");
        }
    }

    rasterize(triClipped, win, renderer);
}


void vertexShaderStageTriangle(TRI in, GWindow* win, GRenderer* renderer) 
{
    V3 v1vs = rotateAroundY(in.v1, rotZ);
    V3 v2vs = rotateAroundY(in.v2, rotZ);
    V3 v3vs = rotateAroundY(in.v3, rotZ);

    v1vs = gmath::translate(v1vs, translation);
    v2vs = gmath::translate(v2vs, translation);
    v3vs = gmath::translate(v3vs, translation);

    cullAndClipStage({ v1vs, v2vs, v3vs }, win, renderer);

}





void do3D(GRenderer* renderer, GWindow* window) 
{
    gmath::V3 v1 = { -0.5, -0.5, 0 };
    gmath::V3 v2 = { 0.5, -0.5, 0 };
    gmath::V3 v3 = { -0.5, 0.5, 0 };
    //gmath::V3 v4 = { 0.5, 0.5, 0 };

    vertexShaderStageTriangle({ v1, v2, v3 }, window, renderer);


//    v1 = gmath::rotateAroundX(v1, rotZ);
//    v2 = gmath::rotateAroundX(v2, rotZ);
//    v3 = gmath::rotateAroundX(v3, rotZ);
//    v4 = gmath::rotateAroundX(v4, rotZ);
//
//    v1 = gmath::translate(v1, translation);
//    v2 = gmath::translate(v2, translation);
//    v3 = gmath::translate(v3, translation);
//    v4 = gmath::translate(v4, translation);
//
//    v1 = gmath::perspective(v1);
//    v2 = gmath::perspective(v2);
//    v3 = gmath::perspective(v3);
//    v4 = gmath::perspective(v4);
//
//#ifdef DEBUG_GAME
//    // some test stuff 
//    gmath::Ray ray = { {0, 1, 0}, {1, 1, -2}, 100 };
//    gmath::V3 hitPoint = { 0, 0, 0 };
//    bool intersects = gmath::rayIntersectsPlane({ -1, 0, 0 }, { 3, 5, 0 }, ray, hitPoint);
//    assert(!intersects);
//    printf("hitpoint: %f/%f/%f\n", hitPoint.x, hitPoint.y, hitPoint.z);
//#endif
//    
//    // end test
//
//    // TODO (gru): clip before the viewport transformation. 
//    gmath::LINE l = { v2, v4 };
//    if (!gmath::isLineTotallyInside(l) && !gmath::isLineTotallyOutside(l)) {
//        l = gmath::clipLineToUnitCube(l);
//        v2 = l.start;
//        v4 = l.end;
//    }
//
//    gmath::V3 vvp1 = gmath::viewportTransform(window, v1);
//    renderer->setPixel(vvp1.x, vvp1.y, { 1, .2, 0.8, 1 });
//    gmath::V3 vvp2 = gmath::viewportTransform(window, v2);
//    renderer->setPixel(vvp2.x, vvp2.y, { .9, 1, 0.8, 1 });
//    gmath::V3 vvp3 = gmath::viewportTransform(window, v3);
//    renderer->setPixel(vvp3.x, vvp3.y, { .9, .2, 0.8, 1 });
//    gmath::V3 vvp4 = gmath::viewportTransform(window, v4);
//    renderer->setPixel(vvp4.x, vvp4.y, { .9, .2, 0.8, 1 });
//
//    renderer->drawLine(vvp1.x, vvp1.y, vvp2.x, vvp2.y, { 1, 0, 0, 1 });
//    renderer->drawLine(vvp2.x, vvp2.y, vvp4.x, vvp4.y, { 0.8, 1, .8, 1 });
//    renderer->drawLine(vvp4.x, vvp4.y, vvp1.x, vvp1.y, { 0.1, 0.1, 1, 1 });
//
//    renderer->drawLine(vvp4.x, vvp4.y, vvp3.x, vvp3.y, { 0, 0, 1, 1 });
//    renderer->drawLine(vvp3.x, vvp3.y, vvp1.x, vvp1.y, { 0, 0, 1, 1 });
//    renderer->drawLine(vvp1.x, vvp1.y, vvp4.x, vvp4.y, { 0, 0, 1, 1 });

}

void renderLineSimple(V3 p1, V3 p2, V3 worldTranslation, float worldRotY, float worldRotZ,
        GRenderer* renderer, GWindow* window, GColor col, V3 cameraPosition, float cameraRotationY, float cameraRotationZ) {
    
    // To world space
    V3 p1World = add(p1, worldTranslation);
    V3 p2World = add(p2, worldTranslation);
    V3 psWorld[] = { p1World, p2World };

    // To view space
    V3 p1View = subtract(psWorld[0], cameraPosition);
    V3 p2View = subtract(psWorld[1], cameraPosition);

    V3 p1ViewRY = { 0, p1View.y, 0 };
    V3 p2ViewRY = { 0, p2View.y, 0 };
    cameraRotationY *= -1;

    p1ViewRY.x = p1View.x * cosf(cameraRotationY) + (p1View.z * sinf(cameraRotationY));
    p1ViewRY.z = -(p1View.x * sinf(cameraRotationY)) + p1View.z * cosf(cameraRotationY);
    p2ViewRY.x = p2View.x * cosf(cameraRotationY) + p2View.z * sinf(cameraRotationY);
    p2ViewRY.z = -(p2View.x * sinf(cameraRotationY)) + p2View.z * cosf(cameraRotationY);
    
    V3 psView[] = { p1ViewRY, p2ViewRY };

    V3 psPer[2];

    // Object space clipping
    float zNear = 1.1;

    // Trivial case, the line is completely behind the zNear plane
    if (psView[0].z < zNear && psView[1].z < zNear) {
        return;
    }

    // Trivial case where both points of the line 
    // are in front of the zNear plane, 
    // so can just continue with the perspective divide
    if (psView[0].z >= zNear && psView[1].z >= zNear) {

    }
    else {

        if (psView[0].z >= zNear && psView[1].z < zNear) {
            // clip from p0 to p1
            V3 dir = normalize(subtract(psView[1], psView[0]));
            Ray r = { psView[0], dir, 100 };
            V3 ip = { 0, 0, 0 };
            bool intersects = rayIntersectsPlane({ 0, 0, zNear }, { 0, 0, zNear }, r, ip);
            if (intersects) {
                psView[1] = ip;
            }
        }

        if (psView[0].z < zNear && psView[1].z >= zNear) {
            // clip from p1 to p0
            V3 dir = normalize(subtract(psView[0], psView[1]));
            Ray r = { psView[1], dir, 100 };
            V3 ip = { 0, 0, 0 };
            bool intersects = rayIntersectsPlane({ 0, 0, zNear }, { 0, 0, zNear }, r, ip);
            if (intersects) {
                psView[0] = ip;
            }
        }
    }

    V3 vsPer0 = { psView[0].x / psView[0].z, psView[0].y / psView[0].z, psView[0].z };
    V3 vsPer1 = { psView[1].x / psView[1].z, psView[1].y / psView[1].z, psView[1].z };
    psPer[0] = vsPer0;
    psPer[1] = vsPer1;

    V3 screenV0 = viewportTransform(window, psPer[0]);
    V3 screenV1 = viewportTransform(window, psPer[1]);

    renderer->drawLine(screenV0.x, screenV0.y, screenV1.x, screenV1.y, col);
}

void renderTriSimple(TRI tri, V3 trans, float worldRotY, float worldRotZ ,GRenderer* renderer, GWindow* window, GColor col, V3 cameraPosition, float cameraRotationY, float camRotZ) {
    // To world space
    V3 p1World = add(tri.v1, trans);
    V3 p2World = add(tri.v2, trans);
    V3 p3World = add(tri.v3, trans);
    V3 psWorld[] = { p1World, p2World, p3World };

    // To view space
    V3 p1View = subtract(psWorld[0], cameraPosition);
    V3 p2View = subtract(psWorld[1], cameraPosition);
    V3 p3View = subtract(psWorld[2], cameraPosition);

    V3 p1ViewRY = { 0, p1View.y, 0 };
    V3 p2ViewRY = { 0, p2View.y, 0 };
    V3 p3ViewRY = { 0, p3View.y, 0 };
    cameraRotationY *= -1;

    p1ViewRY.x = p1View.x * cosf(cameraRotationY) + (p1View.z * sinf(cameraRotationY));
    p1ViewRY.z = -(p1View.x * sinf(cameraRotationY)) + p1View.z * cosf(cameraRotationY);
    p2ViewRY.x = p2View.x * cosf(cameraRotationY) + p2View.z * sinf(cameraRotationY);
    p2ViewRY.z = -(p2View.x * sinf(cameraRotationY)) + p2View.z * cosf(cameraRotationY);
    p3ViewRY.x = p3View.x * cosf(cameraRotationY) + p3View.z * sinf(cameraRotationY);
    p3ViewRY.z = -(p3View.x * sinf(cameraRotationY)) + p3View.z * cosf(cameraRotationY);

    V3 psView[] = { p1ViewRY, p2ViewRY, p3ViewRY};

    V3 psPer[3];

    // Object space clipping
    float zNear = 1.1;

    // TODO

    V3 vsPer0 = { psView[0].x / psView[0].z, psView[0].y / psView[0].z, psView[0].z };
    V3 vsPer1 = { psView[1].x / psView[1].z, psView[1].y / psView[1].z, psView[1].z };
    V3 vsPer2 = { psView[2].x / psView[2].z, psView[2].y / psView[2].z, psView[2].z };
    psPer[0] = vsPer0;
    psPer[1] = vsPer1;
    psPer[2] = vsPer2;

    V3 screenV0 = viewportTransform(window, psPer[0]);
    V3 screenV1 = viewportTransform(window, psPer[1]);
    V3 screenV2 = viewportTransform(window, psPer[2]);

    renderer->drawLine(screenV0.x, screenV0.y, screenV1.x, screenV1.y, col);
    renderer->drawLine(screenV1.x, screenV1.y, screenV2.x, screenV2.y, col);
    renderer->drawLine(screenV2.x, screenV2.y, screenV0.x, screenV0.y, col);
}


void do3DSimpleTris(GRenderer* renderer, GWindow* window, V3 camPos, float camRotY)
{

    V3 trans = { 0, 0, 0 };
    V3 cameraPosition = camPos;

    for (int i = -20; i < 20; i++) {
        V3 vlP1 = { i, 0, -30 };
        V3 vlP2 = { i, 0, 30 };
        if (i != 0) {
            renderLineSimple(vlP1, vlP2, trans, 0, 0, renderer, window, { 0.4, 0.4, 0.4, 1 }, cameraPosition, camRotY, 0);
        } 
    }


    for (int i = -20; i < 30; i++) {
        V3 vlP1 = { -40, 0, i };
        V3 vlP2 = { 40, 0, i };
        if (i != 0) {
            renderLineSimple(vlP1, vlP2, trans, 0, 0, renderer, window, { 0.4, 0.4, 0.4, 1 }, cameraPosition, camRotY, 0);
        }        
    }

    // Coordinate axes
    V3 p1 = { 0, 0, -100 };
    V3 p2 = { 0, 0, 100 };
    renderLineSimple(p1, p2, trans, 0, 0, renderer, window, { 0, 0, 1, 1 }, cameraPosition, camRotY, 0);
    p1 = { -100, 0, 0.0 };
    p2 = { 100, 0, .0 };
    renderLineSimple(p1, p2, trans, 0, 0, renderer, window, { 1, 0, 0, 1 }, cameraPosition, camRotY, 0);
    p1 = {0, 100, 0.0 };
    p2 = { 0, -100, .0 };
    renderLineSimple(p1, p2, trans, 0, 0, renderer, window, { 0,.3, 0, 1 }, cameraPosition, camRotY, 0);

    
    V3 t1 = { -0.5, 0, 1 };
    V3 t2 = { 0.5, 0, 1 };
    V3 t3 = { -0.5, 1, 1 };
    TRI tri = { t1, t2, t3 };
    renderTriSimple(tri, trans, 0, 0, renderer, window, { 0.8, 0.8, 0.1, 1 }, cameraPosition, camRotY, 0);
}



void do3DSimple(GRenderer* renderer, GWindow* window, V3 camPos, float camRotY)
{

    //printf("camera pos: %f/%f/%f\n", camPos.x, camPos.y, camPos.z);
    V3 trans = { 0, 0, 0 };
    V3 cameraPosition = camPos;
    

    for (int i = -20; i < 20; i++) {
        V3 vlP1 = { i, 0, -30};
        V3 vlP2 = { i, 0, 30 };
        renderLineSimple(vlP1, vlP2, trans, 0, 0, renderer, window, { 0.4, 0.4, 0.4, 1 }, cameraPosition, camRotY, 0);
    }
    

    for (int i = -20; i < 30; i++) {
        V3 vlP1 = { -40, 0, i };
        V3 vlP2 = { 40, 0, i };
        renderLineSimple(vlP1, vlP2, trans, 0, 0, renderer, window, { 0.4, 0.4, 0.4, 1 }, cameraPosition, camRotY, 0);
    }

   
    // Render a grid
   /* for (float i = -10; i < 10; i += 0.25) {
       
        V3 p1 = { (float)i, y, 1 };
        V3 p2 = { (float)i,  y, 10 };
        renderLineSimple(p1, p2, renderer, window, { 0.2, 0.2, 0.2, 1 });
    }*/

    //for (float i = -10; i < 10; i++) {

    //    V3 p1 = { (float)i, y, 1 };
    //    V3 p2 = { (float)i,  y, 10 };
    //    renderLineSimple(p1, p2, renderer, window, { 0.4, 0.4, 0.4, 1 });
    //}

    //for (int i = 1; i < 50; i++) {
    //    V3 p1 = { -30, y, (float)(i * 0.25) };
    //    V3 p2 = { 30, y, (float)(i * 0.25) };
    //    //renderLineSimple(p1, p2, renderer, window, { 0.2, 0.2, 0.2, 1 });
    //    p1 = { -30, y, (float)(i) };
    //    p2 = { 30, y, (float)(i) };
    //    renderLineSimple(p1, p2, renderer, window, { 0.4, 0.5, 0.4, 1 });
    //   
    //    
    //}

    V3 p1 = { 1, 0, 3 };
    V3 p2 = { 1, 0, -3 };
    renderLineSimple(p1, p2, trans, 0, 0, renderer, window, { 1, 0, 0, 1 }, cameraPosition, camRotY, 0);

    p1 = { -.5, 0, 0.0 };
    p2 = { .5, 0, .0 };
    renderLineSimple(p1, p2, trans, 0, 0, renderer, window, { 0.4, .2, 1, 1 }, cameraPosition, camRotY, 0);
}

void drawLines(GRenderer* renderer) 
{
    // Draw some pixels
    for (int i = 200; i < 600; i++) {
    //    renderer->setPixel(i, 400, { 0.2, 0.5, 1, 1 });
    }

    renderer->setPixel(0, 0, { 1, 1, 1, 1 });


    // Render a star for all possible directions
    GColor lc = { 0.5, 1, 0.2, 1 };
    renderer->drawLine(400, 300, 400, 600, lc);
    renderer->drawLine(400, 300, 500, 600, lc);
    renderer->drawLine(400, 300, 800, 600, lc);
    renderer->drawLine(400, 300, 800, 500, lc);
    renderer->drawLine(400, 300, 800, 300, lc);
    renderer->drawLine(400, 300, 800, 250, lc);
    renderer->drawLine(400, 300, 800, 150, lc);
    renderer->drawLine(400, 300, 800, 50, lc);
    renderer->drawLine(400, 300, 600, 0, lc);
    renderer->drawLine(400, 300, 400, 0, lc);
    renderer->drawLine(400, 300, 0, 0, lc);
    renderer->drawLine(400, 300, 0, 50, lc);
    renderer->drawLine(400, 300, 0, 150, lc);
    renderer->drawLine(400, 300, 0, 300, lc);
    renderer->drawLine(400, 300, 0, 300, lc);
    renderer->drawLine(400, 300, 0, 350, lc);
    renderer->drawLine(400, 300, 0, 400, lc);
    renderer->drawLine(400, 300, 0, 500, lc);
    renderer->drawLine(400, 300, 0, 600, lc);
   
}

LARGE_INTEGER getFrequency() {
    LARGE_INTEGER freq;
    QueryPerformanceFrequency(&freq);
    return freq;
}

LARGE_INTEGER getCounter() {
    LARGE_INTEGER c;
    QueryPerformanceCounter(&c);
    return c;
}

bool doMatrixTests() {
    M4 m1 = M4::One();
    M4 m2 = M4::id();
    M4 mr = m1 * m2;

    bool allOk = true;

    allOk = m1.m11 == 1;
    allOk = m1.m21 == 0;
    allOk = m1.m22 == 1;

    allOk = mr.e[0] == 1;
    allOk = mr.e[5] == 1;
    allOk = mr.e[11] == 1;
    allOk = mr.e[15] == 1;

    return allOk;
    
}

#define DEBUG_GAME_
int main(int argc, char** args)
{

    assert(doMatrixTests());

    LARGE_INTEGER freq = getFrequency();
    LARGE_INTEGER start = getCounter();
    LARGE_INTEGER end = getCounter();

#ifdef DEBUG_GAME
    printf("freq: %d counts per second.\n",(int) freq.QuadPart);
#endif

    GWindow window(800, 600, false);
    GRenderer* renderer = createRenderer(RendererType::SW, &window);
    GInput* input = getInputImpl(&window);

    float fps = 100;
    float ft = 1. / fps;
    int desiredFTInMicros = ft * 1000 * 1000.;

    bool running = true;
    V3 camPos = { 0, 2, -5 };
    float camRotY = 0;
    V3 camFwd = { 0, -2, 5 };
    while (running) {
        
        MSG msg = window.pollMessages();
        if (msg.message == WM_QUIT) {
            running = false;
        }

        V3 camFwdTemp = normalize(camFwd);
        camFwdTemp.x = camFwd.x * cosf(camRotY) + (camFwd.z * sinf(camRotY));
        camFwdTemp.z = -(camFwd.x * sinf(camRotY)) + camFwd.z * cosf(camRotY);
        V3 nf = normalize(camFwdTemp);


        input->pollKeyboard();
        if (input->keyPressed(KEY::A)) {
            V3 right = normalize(cross({ 0, 1, 0 }, normalize(nf)));
            right = multiplyScalar(right, 0.1);
            camPos = subtract(camPos, right);
        }
        if (input->keyPressed(KEY::D)) {
            V3 right = normalize(cross({ 0, 1, 0 }, normalize(nf)));
            right = multiplyScalar(right, 0.1);
            camPos = add(camPos, right);
        }
        if (input->keyPressed(KEY::W)) {
            
            V3 temp = normalize({ nf.x, 0, nf.z });
            temp = multiplyScalar(temp, 0.1);
            camPos = add(camPos, temp);
        }
        if (input->keyPressed(KEY::S)) {
            V3 temp = normalize({ nf.x, 0, nf.z });
            temp = multiplyScalar(temp, 0.1);
            camPos = subtract(camPos, temp);
        }

        if (input->keyPressed(KEY::Q)) {
       
            camRotY -= 0.01;
        }

        if (input->keyPressed(KEY::E)) {
            camRotY += 0.01;
        }

        renderer->clearBlack();
        //drawLines(renderer);
        //do3D(renderer, &window);
        
        //do3DSimple(renderer, &window, camPos, camRotY);
        do3DSimpleTris(renderer, &window, camPos, camRotY);
        renderer->present();

        // Timing stuff
        {
            end = getCounter();
            int tempStart = start.QuadPart;
            start = getCounter();
            int diff = end.QuadPart - tempStart;
            float diffInMicros = (float)diff * 1000 * 1000 / freq.QuadPart;
#ifdef DEBUG_GAME
            printf("frameTime: %d %f \n", diff, diffInMicros);
#endif
            if (diffInMicros < desiredFTInMicros) {
                Sleep((desiredFTInMicros - diffInMicros) / 1000);
            }
        }
        
    }
    
    return 0;
}

