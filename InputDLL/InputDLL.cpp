// InputDLL.cpp : Defines the exported functions for the DLL.
//

#include "framework.h"
#include "InputDLL.h"
#include <Windows.h>
#include <dinput.h>
#include <stdio.h>
#include "../GraphicsDLL/ggraphics.h"

// This is an example of an exported variable
INPUTDLL_API int nInputDLL=0;

// This is an example of an exported function.
INPUTDLL_API int fnInputDLL(void)
{
    return 0;
}


GInput* getInputImpl(GWindow* win)
{
	HMODULE dll = LoadLibrary(L"DInput8DLL.dll");
	if (!dll) {
		return nullptr;
	}
	typedef GInput* (*create)(GWindow*);
	create func;
	func = (create)GetProcAddress(dll, "instantiate");
	if (func) {
		return func(win);
	}
}