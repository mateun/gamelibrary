// The following ifdef block is the standard way of creating macros which make exporting
// from a DLL simpler. All files within this DLL are compiled with the INPUTDLL_EXPORTS
// symbol defined on the command line. This symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see
// INPUTDLL_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
#ifdef INPUTDLL_EXPORTS
#define INPUTDLL_API __declspec(dllexport)
#else
#define INPUTDLL_API __declspec(dllimport)
#endif

class GWindow;

enum class KEY {
	A,
	B,
	C,
	D, E, F, G, H, I, J, K, 
	L, M, N, O, P, Q, R, S, 
	T, U, V, W, X, Y, Z, 
	LEFT, RIGHT, 
	UP, DOWN,
	ENTER, SPC, DEL, BACKSPC,
	CTRL_RIGHT, CTRL_LEFT
};

// This class is exported from the dll
class GInput {
public:
	virtual void pollKeyboard() = 0;
	virtual bool keyPressed(KEY key) = 0;
};


INPUTDLL_API GInput* getInputImpl(GWindow*);


