// GamePlayDLL.cpp : Defines the exported functions for the DLL.
//

#include "framework.h"
#include "GamePlayDLL.h"


// This is an example of an exported variable
GAMEPLAYDLL_API int nGamePlayDLL=0;

int internalFunc()
{
    return 12;
}

// This is an example of an exported function.
GAMEPLAYDLL_API int fnGamePlayDLL(void)
{
    return 5 * internalFunc();
}

// This is the constructor of a class that has been exported.
CGamePlayDLL::CGamePlayDLL()
{
    return;
}
