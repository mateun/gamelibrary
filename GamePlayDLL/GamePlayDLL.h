// The following ifdef block is the standard way of creating macros which make exporting
// from a DLL simpler. All files within this DLL are compiled with the GAMEPLAYDLL_EXPORTS
// symbol defined on the command line. This symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see
// GAMEPLAYDLL_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
#ifdef GAMEPLAYDLL_EXPORTS
#define GAMEPLAYDLL_API __declspec(dllexport)
#else
#define GAMEPLAYDLL_API __declspec(dllimport)
#endif

// This class is exported from the dll
class GAMEPLAYDLL_API CGamePlayDLL {
public:
	CGamePlayDLL(void);
	// TODO: add your methods here.
};

extern GAMEPLAYDLL_API int nGamePlayDLL;

int internalFunc();

GAMEPLAYDLL_API int fnGamePlayDLL(void);


